﻿using UnityEngine;
using System.Collections;

public class MoveTo : MonoBehaviour
{

    public Transform goalA;
    //public Transform goalB;
    //public Transform goalC;
    public Transform origin;

    public void MoveToA()
    {
        NavMeshAgent agent = GetComponent<NavMeshAgent>();
        agent.destination = goalA.position;
    }
    //public void MoveToB()
    //{
    //    NavMeshAgent agent = GetComponent<NavMeshAgent>();
    //    agent.destination = goalB.position;
    //}
    //public void MoveToC()
    //{
    //    NavMeshAgent agent = GetComponent<NavMeshAgent>();
    //    agent.destination = goalC.position;
    //}
    public void MoveToOrigin()
    {
        NavMeshAgent agent = GetComponent<NavMeshAgent>();
        agent.destination = origin.position;
    }
    public void ExitApplication()
    {
        Application.Quit();
    }
    public void Start()
    {
        //MoveToA();
    }
}